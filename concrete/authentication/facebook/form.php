<?php defined('C5_EXECUTE') or die('Access denied.') ?>
<?php if (isset($message)) { ?>
    <div class="alert alert-success" role="alert"><?php echo $message ?></div>
<?php } ?>
<button class="btn btn-block btn-success authFacebookLogin"><?php echo t('Log in with Facebook') ?></button>
<script type="text/javascript">
    $('button.authFacebookLogin').click(function () {
        var login = window.open('<?php echo $loginUrl?>', 'Log in with Facebook', 'width=500,height=300');
        (login.focus && login.focus());

        function loginStatus() {
            if (login.closed) {
                window.location.href = '<?php echo $statusURI?>';
                return;
            }
            setTimeout(loginStatus, 500);
        }

        loginStatus();
        return false;
    });
</script>
